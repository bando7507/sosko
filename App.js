import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import Navigator from './src/navigator/Navigator';
import { useMemo } from 'react';
export default function App() {

  const theme = useMemo(() => ({
    ...DefaultTheme,
    colors: {
    ...DefaultTheme.colors,
      primary: '#fff',
      background: '#fff',
      secondary: '#2ecc71',
      accent: '#ecf0f1',
      black: '#000',
    },
    fontSizes :{
      ...DefaultTheme,
      fontBaseSize: 16,
      fontSmallSize: 14,
      fontMediumSize: 18,
      fontLargeSize: 20,
      fontXLargeSize: 24,
    },
    AllSizes: {
      ...DefaultTheme,
      __mb_10: 10, 
      __mb_15: 15, 
      __mb_20: 20, 
      __mb_30: 30, 
      __mb_35: 35, 
      __mb_40: 40, 
      __mb_45: 45, 
      __mb_50: 50, 
      __mb_100: 100, 

    }
  }),[])
  return (
    <NavigationContainer theme={theme}>
      <Navigator />
    </NavigationContainer>
  );
}


