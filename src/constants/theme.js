import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const dimensions = {
    // app dimensions
    width,
    height
};

export const imgs = {
    home: require("../assets/img/home.png")
}


