import { View, Text } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from '../screens/HomeScreen'
import SplashScreen from '../screens/SplashScreen'
import LoginScreen from '../screens/authentication/LoginScreen'
import SignInScreen from '../screens/authentication/SignInScreen'
import VerifScreen from '../screens/authentication/VerifScreen'

const Navigator = () => {

  const Stack = createStackNavigator()
  return (
    <Stack.Navigator initialRouteName='SplashScreen'>
      <Stack.Screen 
      options={{
        headerShown: false
      }}
      name='SplashScreen'
      component={SplashScreen}
      />
      <Stack.Screen 
      name='HomeScreen'
      component={HomeScreen}
      options={{ headerShown: false }}
      />

      <Stack.Screen 
      name='LoginScreen'
      component={LoginScreen}
      options={{ headerShown: false }}
      />

      <Stack.Screen 
      name='SignInScreen'
      component={SignInScreen}
      options={{ headerShown: false }}
      />
      <Stack.Screen 
      name='VerifScreen'
      component={VerifScreen}
      options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

export default Navigator