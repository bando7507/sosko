import { View, Text } from 'react-native'
import React, { useEffect } from 'react'
import { BackHandler, Image } from 'react-native'
import { dimensions, imgs } from '../constants/theme'
import { useTheme } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Alert } from 'react-native'

const HomeScreen = ({ navigation }) => {

  const theme = useTheme();

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to go back?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <SafeAreaView style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-start',
      gap: 80,
      marginHorizontal: theme.AllSizes.__mb_30,
      backgroundColor: theme.colors.background,
      position: 'relative',
      paddingVertical: 70 
    }}>
      <View>
        <Text style={{
          color: theme.colors.secondary,
          fontSize: theme.AllSizes.__mb_50,
          fontWeight: 'bold',
          textAlign: 'center'
        }}>
          SOSKO
        </Text>
        <Text style={{
          marginTop: theme.AllSizes.__mb_10,
          fontSize: theme.fontSizes.fontMediumSize,
          textAlign: 'center'
        }}>
          Welcome to <Text style={{color: theme.colors.secondary,}}>SOSKO</Text> Let's shop together
        </Text>
      </View>
      <View style={{width: '100%', justifyContent: 'center'}}>
        <Image
        source={imgs.home}
        style={{width: 350 , height: 350 }}
        />
      </View>

      <View style={{
        position: 'absolute',
        width: '100%',
        bottom: '5%'
        }}>
        <TouchableOpacity style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: theme.colors.secondary,
          paddingVertical: theme.AllSizes.__mb_20,
          borderRadius: theme.AllSizes.__mb_15
        }}
        onPress={() => navigation.navigate('LoginScreen')}
        >
          <Text style={{
            color: theme.colors.primary,
            fontSize: theme.AllSizes.__mb_20,
            fontWeight: 'bold',
            textAlign: 'center'
          }}>
            Let's Start
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

export default HomeScreen