import { View, Text } from 'react-native'
import React, { useEffect } from 'react'
import { Feather } from '@expo/vector-icons'; 
import { SafeAreaView } from 'react-native';
import { useTheme } from '@react-navigation/native';

const SplashScreen = ({ navigation }) => {
    const theme = useTheme();
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('HomeScreen')
        }, 2000)
    },[])

  return (
    <SafeAreaView style={{flex: 1, alignItems: 'center', justifyContent: 'center', position: 'relative'}}>
        <View style={{
            backgroundColor: theme.colors.secondary,
            padding: 20,
            borderRadius: 100
        }}>
            <Feather name="shopping-bag" size={54} color="white" />
        </View>
        <View style={{
            marginTop: 10
        }}>
            <Text style={{
                color: theme.colors.secondary,
                fontSize: theme.AllSizes.__mb_40,
                fontWeight: 'bold',
                textAlign: 'center'
            }}>
                SOSKO
            </Text>
        </View>

        <View
        style={{
            width: 100,
            height: 100,
            backgroundColor: theme.colors.secondary,
            borderRadius: 100,
            position: 'absolute',
            bottom: -35,
            right: -35
        }}
        />
        <View
        style={{
            width: 100,
            height: 100,
            backgroundColor: theme.colors.secondary,
            borderRadius: 100,
            position: 'absolute',
            top: -35,
            left: -35
        }}
        />
    </SafeAreaView>
  )
}

export default SplashScreen