import { View, Text, TouchableOpacity } from 'react-native'
import React, { useEffect, useMemo, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useTheme } from '@react-navigation/native'
import { TextInput } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import { AntDesign } from '@expo/vector-icons'; 
import { Feather } from '@expo/vector-icons'; 
import { BackHandler } from 'react-native'

const SignInScreen = ({ navigation }) => {
    const theme = useTheme()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [showpass, setShowpass] = useState(true)
    const [userProfil, setUserProfil] = useState('')


      useEffect(() => {

        const handleBackButtonClick = ()  =>{
          navigation.goBack();
          return true;
        }

        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          handleBackButtonClick
        );

        
        return () => backHandler.remove();
        
      },[])

  return (
    
    <SafeAreaView style={{
      flex: 1, 
      paddingHorizontal: theme.AllSizes.__mb_30,
      justifyContent: 'flex-start',

      position: 'relative',
    }}>

      {/* ...............Head................. */}
      <View style={{
        marginBottom: 5, 
        marginTop: 70
      }}>
        <Text style={{
            fontSize: theme.AllSizes.__mb_30,
            fontWeight: 'bold',
        }}>
            Welcome to
        </Text>
      </View>
      <View>
        <Text style={{
            fontSize: theme.AllSizes.__mb_35,
            fontWeight: 'bold',
            color: theme.colors.secondary,
        }}>
            SOSKO
        </Text>
      </View>


      {/* .............Login form............... */}
      <View style={{
        width: '100%', 
        gap: theme.AllSizes.__mb_20, 
        marginTop: theme.AllSizes.__mb_40
      }}>
        <View style={{
            flexDirection: 'row', 
            alignItems: 'center', 
            gap: 10,
            height: 60,
            borderRadius: 10,
            backgroundColor: theme.colors.accent,
            paddingHorizontal: theme.AllSizes.__mb_15
            }}>
                
            <AntDesign name="user" size={24} color="black" /> 
            <TextInput
                style={{
                    flex: 1
                }}
                value={userProfil}
                onChangeText={(text) => setUserProfil(text)}
                placeholder="Your username"
                leftIcon={<Entypo name="mail" size={24} color="black" />}
            />
        </View>
        <View style={{
            flexDirection: 'row', 
            alignItems: 'center', 
            gap: 10,
            height: 60,
            borderRadius: 10,
            backgroundColor: theme.colors.accent,
            paddingHorizontal: theme.AllSizes.__mb_15
            }}>
                
            <Entypo name="mail" size={24} color="black" />  
            <TextInput
                style={{
                    flex: 1
                }}
                value={email}
                onChangeText={(text) => setEmail(text)}
                placeholder="Email"
                leftIcon={<Entypo name="mail" size={24} color="black" />}
            />
        </View>
        <View style={{
            flexDirection: 'row', 
            alignItems: 'center', 
            gap: 10,
            height: 60,
            borderRadius: 10,
            backgroundColor: theme.colors.accent,
            paddingHorizontal: theme.AllSizes.__mb_15
            }}>
                
              <AntDesign name="lock1" size={24} color="black" />
            <TextInput
                style={{
                    flex: 1
                }}
                secureTextEntry={showpass}
                placeholder="Password"
                value={password}
                onChangeText={(text) => setPassword(text)}
                leftIcon={<Entypo name="mail" size={24} color="black" />}
            />
            <TouchableOpacity 
            onPress={() => setShowpass(!showpass)}
            >
              {showpass ? (
                <Feather name="eye" size={24} color="black" />
              ) : (
                <Feather name="eye-off" size={24} color="black" />
              )}
            </TouchableOpacity>
        </View>

      </View>

      {/* button log / */}
      <View style={{
        flex: 1,
        width: '100%', 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: theme.AllSizes.__mb_40,
        position: 'relative',
      }}>
        <View style={{
          position: 'absolute',
          width: '100%',
          bottom: '20%'
          
          }}>
          <TouchableOpacity style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.colors.secondary,
            paddingVertical: theme.AllSizes.__mb_20,
            borderRadius: theme.AllSizes.__mb_15
          }}
          onPress={() => navigation.navigate('VerifScreen')}
          >
            <Text style={{
              color: theme.colors.primary,
              fontSize: theme.AllSizes.__mb_20,
              fontWeight: 'bold',
              textAlign: 'center'
            }}>
              Let's Start
            </Text>
          </TouchableOpacity>

          <View style={{
            flexDirection: 'row',
            gap: 5,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: theme.AllSizes.__mb_15
          }}>
            <Text>
              You do not have an account
            </Text>
            <TouchableOpacity 
            onPress={() => {navigation.navigate('LoginScreen')}}
            >
              <Text>
                <Text style={{
                  color: theme.colors.secondary,
                  fontWeight: 'bold',
                  textAlign: 'center'
                }}>
                  Log in
                </Text>
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default SignInScreen